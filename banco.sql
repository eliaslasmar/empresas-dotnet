CREATE TABLE [dbo].[Enterprise] (
    [EnterpriseCode] [int] NOT NULL IDENTITY,
    [EnterpriseTypeCode] [int] NOT NULL,
    [Cnpj] [varchar](19) NOT NULL,
    [Name] [varchar](100) NOT NULL,
    CONSTRAINT [PK_dbo.Enterprise] PRIMARY KEY ([EnterpriseCode])
);

CREATE TABLE [dbo].[EnterpriseType] (
    [EnterpriseTypeCode] [int] NOT NULL IDENTITY,
    [Name] [varchar](100) NOT NULL,
    CONSTRAINT [PK_dbo.EnterpriseType] PRIMARY KEY ([EnterpriseTypeCode])
);

CREATE INDEX [IX_EnterpriseTypeCode] ON [dbo].[Enterprise]([EnterpriseTypeCode]);
ALTER TABLE [dbo].[Enterprise] ADD CONSTRAINT [FK_dbo.Enterprise_dbo.EnterpriseType_EnterpriseTypeCode] FOREIGN KEY ([EnterpriseTypeCode]) REFERENCES [dbo].[EnterpriseType] ([EnterpriseTypeCode]) ON DELETE CASCADE;

CREATE TABLE [dbo].[User] (
    [UserCode] [int] NOT NULL IDENTITY,
    [Login] [varchar](100) NOT NULL,
    [Password] [varchar](100) NOT NULL,
    [Token] [varchar](100),
    [ExpirationDate] [datetime] NOT NULL,
    CONSTRAINT [PK_dbo.User] PRIMARY KEY ([UserCode])
);