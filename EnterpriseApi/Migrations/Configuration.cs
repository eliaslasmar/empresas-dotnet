using EnterpriseApi.Context;
using EnterpriseApi.Models;
using System.Data.Entity.Migrations;

namespace EnterpriseApi.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<Context.ContextApi>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(ContextApi context)
        {
            context.Users.AddOrUpdate(p => p.UserCode,
                new User() { UserCode = 1, Login = "testeapple@ioasys.com.br", Password= "12341234" },
                new User() { UserCode = 2, Login = "eliaslasmar@gmail.com", Password = "123" }
            );

            context.EnterpriseTypes.AddOrUpdate(p => p.EnterpriseTypeCode,
                new EnterpriseType() { EnterpriseTypeCode = 1, Name = "Agro" },
                new EnterpriseType() { EnterpriseTypeCode = 2, Name = "Aviation" },
                new EnterpriseType() { EnterpriseTypeCode = 3, Name = "Biotech" },
                new EnterpriseType() { EnterpriseTypeCode = 4, Name = "Eco" },
                new EnterpriseType() { EnterpriseTypeCode = 5, Name = "Ecommerce" },
                new EnterpriseType() { EnterpriseTypeCode = 6, Name = "Education" },
                new EnterpriseType() { EnterpriseTypeCode = 7, Name = "Fashion" },
                new EnterpriseType() { EnterpriseTypeCode = 8, Name = "Fintech" },
                new EnterpriseType() { EnterpriseTypeCode = 9, Name = "Food" },
                new EnterpriseType() { EnterpriseTypeCode = 10, Name = "Games" },
                new EnterpriseType() { EnterpriseTypeCode = 11, Name = "Health" },
                new EnterpriseType() { EnterpriseTypeCode = 12, Name = "IOT" },
                new EnterpriseType() { EnterpriseTypeCode = 13, Name = "Logistics" },
                new EnterpriseType() { EnterpriseTypeCode = 14, Name = "Media" },
                new EnterpriseType() { EnterpriseTypeCode = 15, Name = "Mining" },
                new EnterpriseType() { EnterpriseTypeCode = 16, Name = "Products" },
                new EnterpriseType() { EnterpriseTypeCode = 17, Name = "Real Estate" },
                new EnterpriseType() { EnterpriseTypeCode = 18, Name = "Service" },
                new EnterpriseType() { EnterpriseTypeCode = 19, Name = "Smart City" },
                new EnterpriseType() { EnterpriseTypeCode = 20, Name = "Social" },
                new EnterpriseType() { EnterpriseTypeCode = 21, Name = "Software" },
                new EnterpriseType() { EnterpriseTypeCode = 22, Name = "Technology" },
                new EnterpriseType() { EnterpriseTypeCode = 23, Name = "Tourism" },
                new EnterpriseType() { EnterpriseTypeCode = 24, Name = "Transport" }
            );

            context.Enterprises.AddOrUpdate(p => p.EnterpriseCode,
                new Enterprise()
                {
                    EnterpriseCode = 1,
                    EnterpriseTypeCode = 2,
                    Cnpj = "25401606000160",
                    Name = "Azul Linhas A�reas"
                },
                new Enterprise()
                {
                    EnterpriseCode = 2,
                    EnterpriseTypeCode = 6,
                    Cnpj = "63074033000104",
                    Name = "Universidade Federal de Lavras"
                },
                new Enterprise()
                {
                    EnterpriseCode = 3,
                    EnterpriseTypeCode = 9,
                    Cnpj = "33204117000146",
                    Name = "McDonalds"
                },
                new Enterprise()
                {
                    EnterpriseCode = 4,
                    EnterpriseTypeCode = 5,
                    Cnpj = "95189155000124",
                    Name = "Netshoes"
                },
                new Enterprise()
                {
                    EnterpriseCode = 5,
                    EnterpriseTypeCode = 5,
                    Cnpj = "20932839000175",
                    Name = "Amazon"
                },
                new Enterprise()
                {
                    EnterpriseCode = 6,
                    EnterpriseTypeCode = 24,
                    Cnpj = "31579161000105",
                    Name = "Expresso Gardenia"
                },
                new Enterprise()
                {
                    EnterpriseCode = 7,
                    EnterpriseTypeCode = 22,
                    Cnpj = "45190937000105",
                    Name = "ioasys - inova��o digital �gil"
                },
                new Enterprise()
                {
                    EnterpriseCode = 8,
                    EnterpriseTypeCode = 23,
                    Cnpj = "28127791000134",
                    Name = "CVC Viagens"
                },
                new Enterprise()
                {
                    EnterpriseCode = 9,
                    EnterpriseTypeCode = 21,
                    Cnpj = "38043859000150",
                    Name = "Microsoft"
                },
                new Enterprise()
                {
                    EnterpriseCode = 10,
                    EnterpriseTypeCode = 14,
                    Cnpj = "12919439000100",
                    Name = "Spotify"
                }
            );
        }
    }
}
