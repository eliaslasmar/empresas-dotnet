﻿using EnterpriseApi.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace EnterpriseApi.Context
{
    public class ContextApi : DbContext
    {
        public ContextApi() : base("SqlServerConnection")
        {

        }

        public DbSet<User> Users { get; set; }

        public DbSet<EnterpriseType> EnterpriseTypes { get; set; }

        public DbSet<Enterprise> Enterprises { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Properties().Where(p => p.Name == p.ReflectedType.Name + "Code").Configure(p => p.IsKey());

            modelBuilder.Properties<string>().Configure(p => p.HasColumnType("varchar"));

            modelBuilder.Properties<string>().Configure(p => p.HasMaxLength(100));
        }
    }
}