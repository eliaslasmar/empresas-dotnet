﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EnterpriseApi.Models
{
    public class Enterprise
    {
        public int EnterpriseCode { get; set; }

        public int EnterpriseTypeCode { get; set; }

        [ForeignKey("EnterpriseTypeCode")]
        public virtual EnterpriseType EnterpriseType { get; set; }

        [Required]
        [MaxLength(14)]
        public string Cnpj { get; set; }

        [Required]
        public string Name { get; set; }
    }
}