﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EnterpriseApi.Models
{
    public class User
    {
        public int UserCode { get; set; }

        [Required]
        public string Login { get; set; }

        [Required]
        public string Password { get; set; }

        public string Token { get; set; }

        public DateTime? ExpirationDate { get; set; }
    }
}