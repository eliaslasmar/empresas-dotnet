﻿using System.ComponentModel.DataAnnotations;

namespace EnterpriseApi.Models
{
    public class EnterpriseType
    {
        public int EnterpriseTypeCode { get; set; }

        [Required]
        public string Name { get; set; }
    }
}