﻿using EnterpriseApi.Models;
using EnterpriseApi.Services;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;

namespace EnterpriseApi.Controllers
{
    public class EnterprisesController : ApiController
    {
        private string client;
        private string uid;
        private string accessToken;

        public IEnumerable<Enterprise> Get(int enterprise_types = 0, string name = null)
        {
            try
            {
                GetHeader();

                if (new UserService().Authenticate(client, uid, accessToken))
                {
                    return new EnterpriseService().List(enterprise_types, name);
                }

                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }
            catch (HttpResponseException hre)
            {
                throw hre;
            }
            catch
            {
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
        }

        public Enterprise Get(int id)
        {
            try
            {
                GetHeader();

                if (new UserService().Authenticate(client, uid, accessToken))
                {
                    return new EnterpriseService().Get(id);
                }

                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }
            catch (HttpResponseException hre)
            {
                throw hre;
            }
            catch
            {
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
        }

        private void GetHeader()
        {
            client = Request.Headers.GetValues("client").FirstOrDefault();
            uid = Request.Headers.GetValues("uid").FirstOrDefault();
            accessToken = Request.Headers.GetValues("access-token").FirstOrDefault();
        }
    }
}