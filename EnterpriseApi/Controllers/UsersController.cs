﻿using EnterpriseApi.Models;
using EnterpriseApi.Services;
using System;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace EnterpriseApi.Controllers
{
    public class UsersController : ApiController
    {
        public object Post()
        {
            try
            {
                string client = Request.Headers.GetValues("client").FirstOrDefault();
                string uid = Request.Headers.GetValues("uid").FirstOrDefault();

                User data = new UserService().Authenticate(client, uid);

                if (data != null)
                {
                    HttpContext.Current.Response.Headers.Add("access-token", data.Token);

                    return new
                    {
                        data.UserCode,
                        data.Login,
                        data.Token
                    };
                }

                return new HttpStatusCodeResult((int)HttpStatusCode.Unauthorized, "Credenciais de acesso inválidas!");
            }
            catch (Exception e)
            {
                return new HttpStatusCodeResult((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }
    }
}