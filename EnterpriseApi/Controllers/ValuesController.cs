﻿using EnterpriseApi.Context;
using EnterpriseApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EnterpriseApi.Controllers
{
    public class ValuesController : ApiController
    {
        private ContextApi Bd;

        // GET api/values
        public IEnumerable<string> Get()
        {
            Bd = new ContextApi();

            var consulta = Bd.Set<EnterpriseType>().AsQueryable();

            int total = consulta.Count();

            var dados = consulta.ToList();

            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
