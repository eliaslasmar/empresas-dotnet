﻿using EnterpriseApi.Context;
using EnterpriseApi.Models;
using System.Collections.Generic;
using System.Linq;

namespace EnterpriseApi.Services
{
    public class EnterpriseService
    {
        private ContextApi context;

        public EnterpriseService()
        {
            context = new ContextApi();
        }

        public Enterprise Get(int code)
        {
            var query = context.Set<Enterprise>().Where(p => p.EnterpriseCode == code).AsQueryable();

            return query.FirstOrDefault();
        }

        public List<Enterprise> List(int enterpriseType, string name)
        {
            var query = context.Set<Enterprise>().AsQueryable();

            if (enterpriseType > 0)
            {
                query = query.Where(p => p.EnterpriseTypeCode == enterpriseType);
            }

            if (!string.IsNullOrEmpty(name))
            {
                query = query.Where(p => p.Name.ToLower().Contains(name.ToLower()));
            }

            return query.ToList();
        }
    }
}