﻿using EnterpriseApi.Context;
using EnterpriseApi.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Configuration;

namespace EnterpriseApi.Services
{
    public class UserService
    {
        private ContextApi context;

        public UserService()
        {
            context = new ContextApi();
        }

        public User Authenticate(string login, string password)
        {
            var query = context.Set<User>().Where(p => p.Login == login && p.Password == password).AsQueryable();

            User data = query.FirstOrDefault();

            if (data != null)
            {
                const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
                Random random = new Random();
                string token = new String(Enumerable.Repeat(chars, 22)
                  .Select(s => s[random.Next(s.Length)]).ToArray());

                data.Token = token;
                data.ExpirationDate = DateTime.Now.AddMinutes(Convert.ToInt32(WebConfigurationManager.AppSettings["SessionTime"]));
                context.Entry(data).State = EntityState.Modified;
                context.SaveChanges();
            }

            return data;
        }

        public bool Authenticate(string login, string password, string token)
        {
            var query = context.Set<User>().Where(p => p.Login == login && p.Password == password && p.Token == token && p.ExpirationDate >= DateTime.Now)
                                            .AsQueryable();

            User data = query.FirstOrDefault();

            if (data != null)
            {
                return true;
            }

            return false;
        }
    }
}